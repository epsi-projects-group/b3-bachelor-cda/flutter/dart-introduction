/// https://dart-tutorial.com/object-oriented-programming/mixins-in-dart/

mixin CanFly {
  void fly() {
    print("I can fly.");
  }
}

mixin CanWalk {
  void walk() {
    print("I can walk.");
  }
}

class Bird with CanFly, CanWalk {}
class Human with CanWalk {}

void main() {
  var b = Bird();
  b.fly();
  b.walk();

  var h = Human();
  h.walk();
}