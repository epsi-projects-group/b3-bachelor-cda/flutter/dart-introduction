/// https://dart-tutorial.com/object-oriented-programming/generics-in-dart/


/// Generic class
class Data<T> {
  T data;
  Data(this.data);
}

/// Generic class with restriction
class DataRestriction<T extends num> {
  T data;
  DataRestriction(this.data);
}

/// Generic method
T genericMethod<T, U>(T value1, U value2) {
  return value1;
}

/// Generic method with restriction
double getAverage<T extends num>(T value1, T value2) {
  return (value1 * value2) / 2;
}

void main() {
  // Create object of type int and double (Data class)
  Data<int> intData = Data<int>(2);
  Data<double> doubleData = Data<double>(5.5);

  // Print the data
  print("intData: ${intData.data}");
  print("doubleData: ${doubleData.data}");

  // Call generic method
  print(genericMethod<int, String>(1, "2"));

  // Create object of type int and double (DataRestriction class)
  DataRestriction<int> intDataRestriction = DataRestriction<int>(2);
  DataRestriction<double> doubleDataRestriction = DataRestriction<double>(5.5);
  // Not possible :
  // DataRestriction<String> stringDataRestriction = DataRestriction<String>("Hello world!");

  // Call generic method with restriction
  print(getAverage<int>(5, 2));
}