import 'dart:math';

/// Custom exception class
class NegativeSquareRootException implements Exception {
  @override
  String toString() {
    return "Square root of negative number is not allowed here.";
  }
}

/// Function to square root a number using custom exception
num squareRoot(int i) {
  if (i < 0) {
    throw NegativeSquareRootException();
  } else {
    return sqrt(i);
  }
}