abstract class Animal {
  /// Properties
  String name;
  double speed;
  /// Constructor
  Animal(this.name, this.speed);
  /// Abstract method
  void run();
}

mixin CanRun on Animal {
  /// Implementation of abstract method
  @override
  void run() => print("$name is Running at speed $speed");
}

class Dog extends Animal with CanRun {
  /// Implement parent constructor
  Dog(super.name, super.speed);
}

void main() {
  var dog = Dog("My pretty dog", 25);
  dog.run();
}