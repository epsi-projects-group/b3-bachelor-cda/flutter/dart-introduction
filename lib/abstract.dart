abstract class Bank {
  /// Properties
  final String _name;
  final double _rate;

  /// Constructor
  Bank(this._name, this._rate);

  /// Getter
  String get name => _name;
  double get rate => _rate;

  /// Abstract method
  void interest();

  /// Non abstract method
  void display() {
    print("Bank Name: $name.");
  }
}

class HSBC extends Bank {
  /// Parent constructor
  HSBC(super.name, super.rate);

  @override
  void interest() {
    print("The rate of interest of HSBC is $rate.");
  }
}

void main() {
  HSBC h = HSBC("HSBC of France", 0.12);
  h.interest();
  h.display();
}