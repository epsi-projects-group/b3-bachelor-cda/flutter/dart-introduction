void main() {
  /// "?" and "!" meaning

  // Give error because can't be null in dart except with "?".
  int productId = null;
  // Okay because "?" added to tell the variable can be null.
  int? productId2 = null;


  String? name;
  // Can't assign the variable of the value "name" bcz she can be null.
  String myName = name;
  // To force the compiler to tell the value is non null add "!".
  String yourName = name!;

  /// Null aware operators

  /// If null operator
  String? address;
  // If the address is null so value of status is "No address" else is the value of address.
  String status = address ?? "No address";

  /// Null aware assignment operator
  String? country;
  // If the value is null then assign value after ?== else don't assign any other value.
  country ??= "FR";
  print(country);

  /// Null aware access operator
  int? age;
  // Return null if age (left side) is null else return the property of the right hand (isNegative)
  print(age?.isNegative);
}