class Car {

  /// Properties
  String? _model;
  int? _year;
  int? _kilometer;

  /// Constructor in short form
  Car(this._model, this._year, this._kilometer);
  /// Constructor with optional parameters
  // Car(this._model, this._year, [this._kilometer = 0]);
  /// Constructor with named parameters
  // Car({this.model, this.year, this.kilometer});
  /// Constructor with default values
  // Car({this.model = "911", this.year = 2000, this.kilometer = 0})
  /// Named constructor
  // Car.namedConstructor(this._model, this._year, this._kilometer)
  /// Constant constructor (need final properties)
  /// (improve performance and can't redefine properties only read)
  // const Car(this.brand);
  /// Constant constructor with named parameters
  // const Car({this.brand})

  /// Getters (with fat arrows)
  String get model => _model!;
  int get year => _year!;
  int get kilometer => _kilometer!;
  /// Getters with Data validation
  /*String get model {
    if (_model!.isEmpty){
      return "No model.";
    }
    return _model!;
  }*/

  /// Setters (with fat arrows)
  set model(String model) => _model = model;
  set year(int year) => _year = year;
  set kilometer(int kilometer) => _kilometer = kilometer;
  /// Setter with Data validation
  /*set year(int year) {
    if (year <= 2000 || year >= DateTime.now().year) {
      throw ArgumentError("Year must be between 2000 and ${DateTime.now().year}");
    }
    _year = year;
  }*/

  /// Method
  void display() {
    print("The car of the model $model, of the year $year which got $kilometer.");
  }
}

class Motorcycle extends Car {

  /// Properties
  final int? _horsePower;

  /// Inheritance
  Motorcycle(String model, int year, int kilometer, this._horsePower) : super(model, year, kilometer);

  /// Getter
  int get horsePower => _horsePower!;

  /// Override parent method (polymorphism)
  @override
  void display() {
    print("The motorcycle of the model $model, of the year $year which got $kilometer, and $horsePower hp.");
  }

  /// Use super keyword to retrieve the parent method in child
  /*void display() {
    print("The horse power $horsePower");
    super.display();
  }*/
}