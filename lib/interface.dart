/// Abstract class as Interface
abstract class Area {
  void area();
}

/// Abstract class as Interface
abstract class Perimeter {
  void perimeter();
}

/// Implements multiple interface
/// PS : Can only extends one class but can implements multiples class
class Rectangle implements Area, Perimeter {
  /// Properties
  int length, breath;

  /// Constructor
  Rectangle(this.length, this.breath);

  /// Implementation of area()
  @override
  void area() {
    print("The area of the rectangle is ${length * breath}.");
  }

  /// Implementation of perimeter()
  @override
  void perimeter() {
    print("The perimeter of the rectangle is ${2 * (length + breath)}.");
  }
}

void main() {
  Rectangle r = Rectangle(10, 20);
  r.area();
  r.perimeter();
}

