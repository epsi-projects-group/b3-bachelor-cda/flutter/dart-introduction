class Global {

  /// Variables
  String name = "John";
  String multiLine = """
  This is a multiline string
  you can write on many lines.
  """;
  String rawString = r"This is a raw string \n, \tshe don't care about return or tab.";

  num age = 20; // Any types of numbers
  double height = 1.68;
  int weight = 15;
  bool isTired = false;

  var isPermissive = "Yes of course wtf?"; // Any type of values
  static const nativeCountry = "France";

  /// Collections
  List<String> names = ["John", "Louis", "Lilian", "Nicolas"];
  Set<String> weekday = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
  Map<String, String> myDetails = {
    'name': 'John',
    'address': 'FR',
    'fullName': 'John Doe'
  };

  /// Static function
  static int add(int a, int b) {
    return a + b;
  }

  /// Parameter and return type
  int subtract(int a, int b) {
    return a - b;
  }
  /// No parameter and return type
  String greet() {
    return "Welcome Mr.";
  }

  /// No parameter and no return type
  void sayAge() {
    print(age);
  }

  /// Parameter and no return type
  void writeAge(int age) {
    print(age);
  }

  /// Default value on positional parameter
  void printInformation(String name, String gender, [String title = "sir/myam"]) {
    print("Hello $title $name your gender is $gender.");
  }

  /// Named parameter (need to specify the name of each parameter when calling the func)
  void printAge({int? age}) {
    print("You are $age years old.");
  }

  /// Required Named parameter
  /// Forces the user to call with the required parameters otherwise does not work.
  void printGender({required String gender}) {
    print("You are a $gender.");
  }

  /// Optional parameter
  static void printHeightAndWeight(int height, [int? weight]) {
    if (weight == null) {
      print("You measure $height centimeter.");
    } else {
      print("You measure $height and weigh $weight.");
    }
  }

}