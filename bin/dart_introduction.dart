import 'dart:io';

import 'package:dart_introduction/global.dart';

void main(List<String> arguments) {

  // Check the type of variable
  /*print(Test.nativeCountry.runtimeType);
  print(Test.nativeCountry is String);*/

  // String user input
  /*print("Enter the name:");
  String? name = stdin.readLineSync();
  print("The entered name is $name");*/

  // Integer user input
  /*print("Enter a number:");
  int number = int.parse(stdin.readLineSync()!);
  print("The entered number is $number");*/

  // Convert String to int
  /*String number = "2";
  int numberConverted = int.parse(number);*/

  // Convert int to String
  /*int number = 5;
  String numberConverted = number.toString();*/

  // Assertion
  /*int age = 20;
  assert(age>20, "The age must be greater than 20.");*/

  // Ternary Operator
  /*int num1 = 10;
  int num2 = 15;
  int max = (num1 > num2) ? num1 : num2;
  print(max);*/

  // For Each loop
  /*List<String> footballPlayers = ['Ronaldo', 'Messi', 'Neymar', 'Hazard'];*/
  // First method
  /*for (String player in footballPlayers) {
    print(player);
  }*/
  // Second method
  /*footballPlayers.forEach( (player) => print(player));*/

  // Try catch loop
  /*int a = 12;
  int b = 0;
  int res;
  try {
    res = a ~/ b;
  } on UnsupportedError {
    print ("Can't divide by zero.");
  } catch (ex) {
    print(ex);
  } finally {
    print("Finally block always executed.");
  }*/

  // Throwing an Exception
  /*throw FormatException();
  throw Exception("Blablabla");*/

  // Call Named parameter function
  /*Custom c = Custom();
  c.printAge(age: 12);*/

  // Call Optional parameter function
  /*Custom.printHeightAndWeight(12);
  Custom.printHeightAndWeight(10, 12);*/

  // Anonymous function - 1 (no name and no return type)
  /*const fruits = ["Apple", "Mango", "Banana", "Orange"];
  fruits.forEach((fruit) {
    print(fruit);
  });*/
  // Anonymous function - 2 (no name and no return type)
  /*var cube = (int number) {
    return number * number * number;
  };
  print("The cube of 2 is ${cube(2)}.");*/

  // Arrow function
  /*int add(int a, int b) => a + b;
  int result = add(2, 3);
  print(result);*/

  // Combine two or more List
  /*List<String> names = ["Raj", "John", "Rocky"];
  List<String> otherNames = ["Jacques", "Jules", "Louis"];
  List<String> allNames = [...names, ...otherNames];*/

  // Function map on List
  /*List<int> integers = [10, 20, 30, 40, 50];
  var doubledIntegers = integers.map((e) => e * 2);*/

  // Filter List with function .where()
  /*List<int> numbers = [2, 4, 6, 8, 10, 11, 12, 13, 14];
  List<int> evenNumbers = numbers.where((number) => number.isEven).toList();*/

  // Looping in Map using For Each
  /*Map<String, dynamic> book = {
    'title': 'Misson Mangal',
    'author': 'Kuber Singh',
    'page': 233
  };
  book.forEach((key, value) => print("Key is $key and value is $value"));
  book.forEach((key, value) {
    print("Key is $key and value is $value");
  });*/

  // Remove element in map using function .removeWhere()
 /* Map<String, double> mathMarks = {
    "ram": 30,
    "mark": 32,
    "harry": 88,
    "raj": 69,
    "john": 15,
  };
  mathMarks.removeWhere((key, value) => value < 32);*/

}
